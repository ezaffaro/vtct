import io
import os.path as op

from setuptools import setup, find_packages

here = op.abspath(op.dirname(__file__))

# Get the long description from the README file
with io.open(op.join(here, 'README.md'), mode='rt', encoding='utf-8') as f:
	long_description = f.read()

setup(name='vtct',
	  packages=find_packages(),
	  version='0.1',
	  description='Package to elaborate TCT data.',
	  long_description=long_description,
	  long_description_content_type='text/markdown',
	  url='https://gitlab.cern.ch/ezaffaro/vtct',
	  author='Ettore Zaffaroni',
	  license='MIT',
	  install_requires=['h5py', 'numpy', 'scipy', 'matplotlib'],
	  entry_points={'console_scripts': ['vtct-convert = vtct.tools.convert:main',
										'vtct-merge = vtct.tools.merge:main',
										'vtct-charge = vtct.tools.charge:main',
										'quick-charge = vtct.tools.quick_charge:main',
										'quick-focus = vtct.tools.quick_focus:main'], },
	  python_requires='>=3.4'
	  )

