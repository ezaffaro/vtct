#!/usr/bin/env python3

import argparse
import logging
from os import path
import numpy as np
from scipy.integrate import simps
from ..utils.utils import create_path
import h5py


def main():
	parser = argparse.ArgumentParser(description='Read the HDF5 waveforms file and calculate the charge.')

	parser.add_argument('folderpath', type=str, help='path of the folder containing the waveforms HDF5 file')
	parser.add_argument('start', type=float, help='start integration time, in ns')
	parser.add_argument('end', type=float, help='end integration time, in ns')
	parser.add_argument('--output-path', type=str, help='Output path, if different from folderpath')
	parser.add_argument('--input-filename', type=str, default='data.hdf5')
	parser.add_argument('--output-filename', type=str, default='charge.hdf5')
	parser.add_argument('--time-offset', type=float, nargs='+', help='Time offset for the different channels')
	args = parser.parse_args()

	output_path = args.output_path if args.output_path is not None else args.folderpath
	create_path(output_path)

	logging.basicConfig(level=logging.DEBUG)

	with h5py.File(path.join(args.folderpath, args.input_filename), 'r') as f:
		wfs = f['waveforms']
		metadata = f.attrs

		timebase = np.linspace(start=metadata['t0'],
							stop=metadata['t0']+metadata['dt']*metadata['NP'],
							num=metadata['NP'],
							endpoint=False)

		channels = metadata['channels']
		n_channels = np.count_nonzero(channels == 1)
		NU1 = metadata['NU1']
		NU2 = metadata['NU2']
		Nx = metadata['Nx']
		Ny = metadata['Ny']
		Nz = metadata['Nz']

		if args.time_offset is not None:
			if len(args.time_offset) < n_channels:
				raise IndexError('Option --time-offset must have {} elements.'.format(n_channels))
			elif len(args.time_offset) > n_channels:
				logging.warning('Option --time-offset has more elements than the number of channels. The exceeding ones will be ignored.')

		with h5py.File(path.join(output_path, args.output_filename), 'w') as fout:
			if args.time_offset is None:
				start_bin = (np.abs(timebase - args.start)).argmin()
				end_bin = (np.abs(timebase - args.end)).argmin()
				charge = fout.create_dataset('charge', shape=(n_channels, NU2, NU1, Nz, Ny, Nx), dtype='f4',
											 data=simps(wfs[:, :, :, :, :, :, start_bin:end_bin], timebase[start_bin:end_bin]))
			else:
				charge = fout.create_dataset('charge', shape=(n_channels, NU2, NU1, Nz, Ny, Nx), dtype='f4')
				for i in range(n_channels):
					start_bin = (np.abs(timebase - args.start - args.time_offset[i])).argmin()
					end_bin = (np.abs(timebase - args.end - args.time_offset[i])).argmin()

					charge[i] = simps(wfs[i, :, :, :, :, :, start_bin:end_bin], timebase[start_bin:end_bin])

			for key in metadata.keys():
				fout.attrs[key] = metadata[key]


if __name__ == '__main__':
	main()