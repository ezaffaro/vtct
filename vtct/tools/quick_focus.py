#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import argparse
from ..parser.pstct_parser import parse_data
import logging
from scipy.integrate import simps
from scipy.optimize import curve_fit
from scipy.special import erf


def fitF(x, A, mu, sigma, C):
	return A * erf((x - mu) / sigma) + C


def parable(x, A, x0, C):
	return A * (x - x0) * (x - x0) + C


def get_axes(args, metadata):
	if args.scan_axis == 'x':
		s0 = metadata['x0']
		smax = s0 + metadata['Nx'] * metadata['dx']
		s = np.linspace(s0, smax, num=metadata['Nx'])
	elif args.scan_axis == 'y':
		s0 = metadata['y0']
		smax = s0 + metadata['Ny'] * metadata['dy']
		s = np.linspace(s0, smax, num=metadata['Ny'])
	else:
		s0 = metadata['z0']
		smax = s0 + metadata['Nz'] * metadata['dz']
		s = np.linspace(s0, smax, num=metadata['Nz'])

	if args.focus_axis == 'x':
		f0 = metadata['x0']
		fmax = f0 + metadata['Nx'] * metadata['dx']
		f = np.linspace(f0, fmax, num=metadata['Nx'])
	elif args.focus_axis == 'y':
		f0 = metadata['y0']
		fmax = f0 + metadata['Ny'] * metadata['dy']
		f = np.linspace(f0, fmax, num=metadata['Ny'])
	else:
		f0 = metadata['z0']
		fmax = f0 + metadata['Nz'] * metadata['dz']
		f = np.linspace(f0, fmax, num=metadata['Nz'])

	f = f[args.f_min:args.f_max]
	s = s[args.s_min:args.s_max]

	return s, f


def get_charge(wfs, metadata, args):
	timebase = np.linspace(start=metadata['t0'],
						   stop=metadata['t0'] + metadata['dt'] * metadata['NP'],
						   num=metadata['NP'],
						   endpoint=False)

	start_bin = (np.abs(timebase - args.start)).argmin()
	end_bin = (np.abs(timebase - args.stop)).argmin()

	charge_array = simps(wfs[:, :, :, start_bin:end_bin], timebase[start_bin:end_bin])

	if (args.scan_axis == 'x' and args.focus_axis == 'y') or (args.scan_axis == 'y' and args.focus_axis == 'x'):
		charge_array = charge_array[args.other_param, :, :]
	elif (args.scan_axis == 'x' and args.focus_axis == 'z') or (args.scan_axis == 'z' and args.focus_axis == 'x'):
		charge_array = charge_array[:, args.other_param, :]
	elif (args.scan_axis == 'y' and args.focus_axis == 'z') or (args.scan_axis == 'z' and args.focus_axis == 'y'):
		charge_array = charge_array[:, :, args.other_param]

	if (args.scan_axis == 'y' and args.focus_axis == 'x') \
			or (args.scan_axis == 'z' and args.focus_axis == 'x') \
			or (args.scan_axis == 'z' and args.focus_axis == 'y'):
		charge_array.transpose()

	return charge_array[args.f_min:args.f_max, args.s_min:args.s_max]


def main():
	parser = argparse.ArgumentParser(description='Estimate the focus position.')

	parser.add_argument('filename', type=str, help='Filename')
	# parser.add_argument('mode', type=str, choices=['xy', 'xz', 'yz'])
	parser.add_argument('start', type=int, help='start integration time in ns')
	parser.add_argument('stop', type=int, help='stop integration time in ns')
	parser.add_argument('--t_baseline', '-t', type=float, default=20)
	parser.add_argument('--channel', '-ch', type=int, default=0, choices=[0, 1, 2, 3], help='channel to be used')
	parser.add_argument('--scan-axis', type=str, default='x', choices=['x', 'y', 'z'])
	parser.add_argument('--focus-axis', type=str, default='z', choices=['x', 'y', 'z'])
	parser.add_argument('--s-min', type=int, default=None, help='minimum index along scan axis')
	parser.add_argument('--s-max', type=int, default=None, help='maximum index along scan axis')
	parser.add_argument('--f-min', type=int, default=None, help='minimum index along focus axis')
	parser.add_argument('--f-max', type=int, default=None, help='maximum index along focus axis')
	parser.add_argument('--other-param', type=int, default=0, help='the index of the unused spatial dimension')
	parser.add_argument('--uu1', type=int, default=0, help='the index of U1 to be used')
	parser.add_argument('--uu2', type=int, default=0, help='the index of U2 to be used')

	args = parser.parse_args()

	logging.basicConfig(level=logging.INFO, format='%(levelname)s - %(filename)s: %(message)s')

	if args.focus_axis == args.scan_axis:
		raise ValueError('focus-axis cannot be the same as scan-axis')

	wfs, metadata, _ = parse_data(args.filename, args.t_baseline, select_channels=args.channel,
								  select_uu1=args.uu1, select_uu2=args.uu2)
	wfs = wfs[0, 0, 0]

	charge_array = get_charge(wfs, metadata, args)

	s, f = get_axes(args, metadata)

	sigmas = []
	sigmas_err = []

	for ff in range(charge_array.shape[0]):
		# cycle on each focus point
		A0 = np.max(charge_array[ff, :])
		mu0 = np.mean(s)
		sigma0 = 20
		C0 = 0
		try:
			# fit each charge profile with erf and extract sigma
			fit, cov = curve_fit(fitF, s, charge_array[ff, :], p0=(A0, mu0, sigma0, C0),
								 bounds=((0, s[0], 0, -np.inf), (np.inf, s[-1], s[-1] - s[0], np.inf)))
			sigmas.append(fit[2])
			sigmas_err.append(np.sqrt(np.diag(cov))[2])
		except RuntimeError:
			sigmas.append(float('nan'))
			sigmas_err.append(float('nan'))

	# make the sigmas arrays a numpy array
	sigmas = np.array(sigmas)
	sigmas_err = np.array(sigmas_err)

	# remove NaNs
	f = f[~np.isnan(sigmas)]
	sigmas_err = sigmas_err[~np.isnan(sigmas)]
	sigmas = sigmas[~np.isnan(sigmas)]

	# fit the sigmas vs focus with a parable (maybe not the best?)
	fit, _ = curve_fit(parable, f, sigmas)

	print('Minimum: {:.1f}'.format(fit[1]))
	plt.errorbar(f, sigmas, linestyle='', marker='.', yerr=sigmas_err)
	plt.xlabel('z [um]')
	plt.ylabel('sigma')
	plt.plot(f, parable(f, *fit))
	plt.show()


if __name__ == '__main__':
	main()
