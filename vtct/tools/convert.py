#!/usr/bin/env python3

import argparse
import logging
from os import path
from ..parser.pstct_parser import parse_data
from ..utils.utils import LOGGING_DICT, create_path
import h5py


def convert(input_file, output_path, output_filename, t_baseline, bm_smooth=250, no_bm=False, transpose='xyz', flip='',
			remove_uu1=(), remove_uu2=(), x_min=None, x_max=None, y_min=None, y_max=None, z_min=None, z_max=None,
			t_min=None, t_max=None, select_channels=None):

	wfs, metadata, bm = parse_data(input_file, t_baseline, bm_smooth=bm_smooth, correct_bm=not no_bm,
								   axes_transpose=transpose, axes_flip=flip,
								   remove_uu1=remove_uu1, remove_uu2=remove_uu2,
								   x_min=x_min, x_max=x_max, y_min=y_min, y_max=y_max, z_min=z_min, z_max=z_max,
			t_min=t_min, t_max=t_max, select_channels=select_channels)

	create_path(output_path)

	with h5py.File(path.join(output_path, output_filename), 'w') as datafile:
		datafile.create_dataset('waveforms', data=wfs)
		datafile.create_dataset('beam_monitor', data=bm)

		for key, value in metadata.items():
			logging.debug('{}: {}'.format(key, value))
			datafile.attrs[key] = value


def main():
	parser = argparse.ArgumentParser(description='Read a raw Particulars TCT file and dump the content into a hdf5 file.')

	parser.add_argument('inputfile', type=str, help='Path to the input raw file')
	parser.add_argument('outputpath', type=str, help='Path to the output folder')
	parser.add_argument('t_baseline', type=float, help='Time where the signal starts in ns '
													   '(it becomes t0 in the output file).'
													   'The signal between 0 and t_baseline is used to calculate the '
													   'baseline, if needed.')
	parser.add_argument('--bm-smooth', type=int, default=250,
						help='Samples to be used for the moving average of the beam monitor data.')
	parser.add_argument('--no-bm', action='store_true',
						help='Do NOT correct the waveforms with the beam monitor data.')
	parser.add_argument('--transpose', type=str, default='xyz', help='Exchange x, y and z axes.',
						choices=('xyz', 'xzy', 'yxz', 'yzx', 'zxy', 'zyx'))
	parser.add_argument('--flip', type=str, default='',
						help='Flip one or more axes. Can be any combination of x, y and z')
	parser.add_argument('--remove-uu1', type=int, nargs='+', default=(),
						help='Indexes of the voltages (U1) to be removed.')
	parser.add_argument('--remove-uu2', type=int, nargs='+', default=(),
						help='Indexes of the voltages (U2) to be removed.')
	parser.add_argument('--select-channels', type=int, nargs='+',
						help='Channels to be selected.')
	parser.add_argument('--x-min', type=int)
	parser.add_argument('--x-max', type=int)
	parser.add_argument('--y-min', type=int)
	parser.add_argument('--y-max', type=int)
	parser.add_argument('--z-min', type=int)
	parser.add_argument('--z-max', type=int)
	parser.add_argument('--t-min', type=int)
	parser.add_argument('--t-max', type=int)
	parser.add_argument('--log-level', type=str, choices=['warning', 'info', 'debug'],
						default='info', help='Logging level.')
	parser.add_argument('--output-filename', type=str, default='data.hdf5')

	args = parser.parse_args()

	logging.basicConfig(level=LOGGING_DICT[args.log_level], format='%(levelname)s - %(filename)s: %(message)s')

	convert(args.inputfile, args.outputpath, args.output_filename, args.t_baseline,
			bm_smooth=args.bm_smooth, no_bm=args.no_bm, transpose=args.transpose, flip=args.flip,
			remove_uu1=args.remove_uu1, remove_uu2=args.remove_uu2,
			x_min=args.x_min, x_max=args.x_max, y_min=args.y_min, y_max=args.y_max, z_min=args.z_min, z_max=args.z_max,
			t_min=args.t_min, t_max=args.t_max, select_channels=args.select_channels)


if __name__ == '__main__':
	main()
