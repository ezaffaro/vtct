#!/usr/bin/python3

import numpy as np
import matplotlib.pyplot as plt
import argparse
from ..parser.pstct_parser import parse_data
import logging
from scipy.integrate import simps


def main():
	parser = argparse.ArgumentParser(description='Reads a raw TCT file and displays a 2D plot of the collected charge.')

	parser.add_argument('filename', type=str, help='filename of the raw TCT file')
	parser.add_argument('mode', type=str, choices=['xy', 'xz', 'yz'], help='axes along which the charge will be plotted')
	parser.add_argument('start', type=int, help='start integration time in ns')
	parser.add_argument('stop', type=int, help='stop integration time in ns')
	parser.add_argument('--t_baseline', '-t', type=float, default=20, help='the portion of waveforms, in ns, that will'
																		   ' be used to calculate the baseline')
	parser.add_argument('--channel', '-ch', type=int, default=0, choices=[0, 1, 2, 3], help='channel to be plotted')
	parser.add_argument('--uu1', type=int, default=0, help='the index of U1 to be plotted')
	parser.add_argument('--uu2', type=int, default=0, help='the index of U2 to be plotted')
	parser.add_argument('--other-param', type=int, default=0, help='the index of the spatial dimension not plotted')
	parser.add_argument('--transpose', type=str, default='xyz', help='Exchange x, y and z axes.',
							choices=('xyz', 'xzy', 'yxz', 'yzx', 'zxy', 'zyx'))
	parser.add_argument('--flip', type=str, default='',
							help='Flip one or more axes. Can be any combination of x, y and z')

	args = parser.parse_args()

	logging.basicConfig(level=logging.WARNING, format='%(levelname)s - %(filename)s: %(message)s')

	wfs, metadata, _ = parse_data(args.filename, args.t_baseline, select_uu1=args.uu1, select_uu2=args.uu2,
								  select_channels=args.channel, axes_transpose=args.transpose, axes_flip=args.flip)

	print('sample:', metadata['sample'])
	print('user:', metadata['user'])
	print('date:', metadata['datetime'])
	print('comment:', metadata['comment'])
	print('')
	print('x0:', metadata['x0'], 'Nx:', metadata['Nx'], 'dx:', metadata['dx'])
	print('y0:', metadata['y0'], 'Ny:', metadata['Ny'], 'dy:', metadata['dy'])
	print('z0:', metadata['z0'], 'Nz:', metadata['Nz'], 'dz:', metadata['dz'])
	print('')
	print('t0:', metadata['t0'], 'NP:', metadata['NP'], 'dt:', metadata['dt'])
	print('')
	print('U1:', metadata['U1'], 'I1:', metadata['I1'])
	print('U2:', metadata['U2'], 'I2:', metadata['I2'])

	timebase = np.linspace(start=-args.t_baseline,
							stop=-args.t_baseline+metadata['dt']*metadata['NP'],
							num=metadata['NP'],
							endpoint=False)

	start_bin = (np.abs(timebase - args.start)).argmin()
	end_bin = (np.abs(timebase - args.stop)).argmin()

	charge_array = simps(wfs[0, 0, 0, :, :, :, start_bin:end_bin], timebase[start_bin:end_bin])

	fig, ax = plt.subplots()
	ax.set_title('Charge profile {} V'.format(metadata['U1'][0]))
	x0 = metadata['x0']
	x_max = x0 + metadata['Nx'] * metadata['dx']
	y0 = metadata['y0']
	y_max = y0 + metadata['Ny'] * metadata['dy']
	z0 = metadata['z0']
	z_max = z0 + metadata['Nz'] * metadata['dz']

	x = np.linspace(x0, x_max, num=metadata['Nx']+1)
	y = np.linspace(y0, y_max, num=metadata['Ny']+1)
	z = np.linspace(z0, z_max, num=metadata['Nz']+1)

	if args.mode == 'xy':
		ax.set(xlabel='x [um]', ylabel='y [um]')
		cax = ax.pcolormesh(x, y, charge_array[args.other_param, :, :], cmap='viridis')
	elif args.mode == 'xz':
		ax.set(xlabel='x [um]', ylabel='z [um]')
		cax = ax.pcolormesh(x, z, charge_array[:, args.other_param, :], cmap='viridis')
	else:
		ax.set(xlabel='y [um]', ylabel='z [um]')
		cax = ax.pcolormesh(y, z, charge_array[:, :, args.other_param], cmap='viridis')

	fig.colorbar(cax, ax=ax, label='Charge [au]')

	plt.show()
	plt.close(fig)


if __name__ == '__main__':
	main()
