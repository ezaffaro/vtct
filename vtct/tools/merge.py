#!/usr/bin/env python3

import argparse
import logging
from os import path
import numpy as np
import h5py

from ..utils.utils import create_path


class InconsistentMetadataError(Exception):
	pass


def check_metadata_consistency(metadata_array, field):
	f0 = metadata_array[0][field]
	for m in metadata_array:

		try:
			if m[field] != f0:
				return False
		except ValueError:
			if (m[field] != f0).all():
				return False

	return True


def main():
	parser = argparse.ArgumentParser(description='Merge two or more already converted measurements')

	parser.add_argument('outputpath', type=str)
	parser.add_argument('inputpaths', type=str, nargs='+')
	# TODO allow to order by voltage?
	# TODO allow to delete inputpaths

	args = parser.parse_args()

	logging.basicConfig(level=logging.DEBUG)

	create_path(args.outputpath)

	metadata_array = []

	logging.debug('Reading metadata files')

	infiles = []

	for i, ip in enumerate(args.inputpaths):
		infiles.append(h5py.File(path.join(ip, 'data.hdf5'), 'r'))
		metadata_array.append(infiles[i].attrs)

	to_be_checked = ['channels', 'Nx', 'Ny', 'Nz', 'x0', 'y0', 'z0', 'dx', 'dy', 'dz', 'NP', 't0', 'dt', 'sample']
	not_to_be_checked = ['user', 'comment', 'datetime', 'NU1', 'NU2']

	new_metadata = {'U1': [], 'U2': [], 'I1': [], 'I2': [], 'NU1': 0, 'NU2': 1}

	logging.info('Checking metadata')
	for i in to_be_checked:
		logging.debug('Checking consistency of ' + i)
		if not check_metadata_consistency(metadata_array, i):
			logging.error('Metadata non consistent. ' + i + ' does not match')
			raise InconsistentMetadataError
		new_metadata[i] = metadata_array[0][i]

	logging.debug('Checking NU2 and preparing voltage and current arrays')
	for m in metadata_array:
		if m['NU2'] != 1:  # can currently merge only if there was only one voltage scan
			logging.error('NU2 != 1. Can currently merge only if NU2 == 1')
			raise NotImplementedError
		new_metadata['U1'].extend(m['U1'])
		new_metadata['I1'].extend(m['I1'])
		new_metadata['NU1'] += m['NU1']

	new_metadata['U2'].extend(metadata_array[0]['U2'])
	new_metadata['I2'].extend(metadata_array[0]['I2'])

	new_metadata['user'] = metadata_array[0]['user']
	new_metadata['comment'] = metadata_array[0]['comment']
	new_metadata['datetime'] = metadata_array[0]['datetime']

	logging.info('Metadata ok.')

	with h5py.File(path.join(args.outputpath, 'data.hdf5'), 'w') as outf:
		dset = outf.create_dataset('waveforms', shape=(np.count_nonzero(new_metadata['channels'] == 1), new_metadata['NU2'], new_metadata['NU1'], new_metadata['Nz'], new_metadata['Ny'], new_metadata['Nx'], new_metadata['NP']), dtype='f4')
		uu1 = 0
		for inf in infiles:
			dset[:, :, uu1:uu1+inf.attrs['NU1'], :, :, :, :] = inf['waveforms']
			uu1 += inf.attrs['NU1']
			inf.close()

		for key, value in new_metadata.items():
			outf.attrs[key] = value

