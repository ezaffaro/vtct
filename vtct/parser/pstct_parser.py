import logging
import numpy as np
import struct
from math import ceil

_SUPPORTED_VERSIONS = [51, 81, 82]

_TRANSPOSE_DICT_WFS = {
	'xyz': (0, 1, 2, 3, 4, 5, 6),
	'xzy': (0, 1, 2, 4, 3, 5, 6),
	'yxz': (0, 1, 2, 3, 5, 4, 6),
	'yzx': (0, 1, 2, 5, 3, 4, 6),
	'zxy': (0, 1, 2, 4, 5, 3, 6),
	'zyx': (0, 1, 2, 5, 4, 3, 6)
}

_TRANSPOSE_DICT_BM = {
	'xyz': (0, 1, 2, 3, 4),
	'xzy': (0, 1, 3, 2, 4),
	'yxz': (0, 1, 2, 4, 3),
	'yzx': (0, 1, 4, 2, 3),
	'zxy': (0, 1, 3, 4, 2),
	'zyx': (0, 1, 4, 3, 2)
}


class UnsupportedVersionError(Exception):
	pass


def _moving_average(array, samples):
	if samples == 0:
		return
	for i in range(array.shape[0]):
				array[i] = np.mean(array[max(0, i - samples):min(array.shape[0], i + samples)])


def _read_float(file):
	return struct.unpack('f', file.read(4))[0]


def _read_float_to_int(file):
	return int(struct.unpack('f', file.read(4))[0])


def _read_int(file):
	return struct.unpack('i', file.read(4))[0]


def _check_args_initial(select_channels, remove_uu1, remove_uu2, select_uu1, select_uu2,
						x_min, x_max, y_min, y_max, z_min, z_max, t_min, t_max, axes_transpose):
	# select_channels, remove_uu1, remove_uu2 can be int, list or tuples
	if isinstance(select_channels, int):
		select_channels = (select_channels,)
	elif not isinstance(select_channels, (list, tuple)) and select_channels is not None:
		raise TypeError('remove_channels must be an int, tuple or list')

	if isinstance(remove_uu1, int):
		remove_uu1 = (remove_uu1,)
	elif not isinstance(remove_uu1, (list, tuple)):
		raise TypeError('remove_uu1 must be an int, tuple or list. It is {}'.format(remove_uu1))

	if isinstance(remove_uu2, int):
		remove_uu2 = (remove_uu2,)
	elif not isinstance(remove_uu2, (list, tuple)):
		raise TypeError('remove_uu2 must be an int, tuple or list. It is {}'.format(remove_uu2))

	if not isinstance(select_uu1, int) and select_uu1 is not None:
		raise TypeError('select_uu1 must be an int.')

	if not isinstance(select_uu2, int) and select_uu2 is not None:
		raise TypeError('select_uu2 must be an int.')

	if (not isinstance(x_min, int) and x_min is not None) \
			or (not isinstance(x_max, int) and x_max is not None) \
			or (not isinstance(y_min, int) and y_min is not None) \
			or (not isinstance(y_max, int) and y_max is not None) \
			or (not isinstance(z_min, int) and z_min is not None) \
			or (not isinstance(z_max, int) and z_max is not None) \
			or (not isinstance(t_min, int) and t_min is not None) \
			or (not isinstance(t_max, int) and t_max is not None):
		raise TypeError('min and max indices must be integers')

	if axes_transpose not in _TRANSPOSE_DICT_WFS.keys():
		raise ValueError('{} is not valid for axes_transpose'.format(axes_transpose))

	return select_channels, remove_uu1, remove_uu2


def _check_args_values_xyz(x_min, x_max, y_min, y_max, z_min, z_max, Nx, Ny, Nz):
	if x_min is not None and (x_min < 0 or x_min > Nx):
		logging.error('x_min must be between 0 and {}. Current value: {}'.format(Nx, x_min))
		raise ValueError('x_min must be between 0 and {}. Current value: {}'.format(Nx, x_min))

	if x_max is not None and (x_max < 0 or x_max > Nx):
		logging.error('x_max must be between 0 and {}. Current value: {}'.format(Nx, x_max))
		raise ValueError('x_max must be between 0 and {}. Current value: {}'.format(Nx, x_max))

	if y_min is not None and (y_min < 0 or y_min > Ny):
		logging.error('y_min must be between 0 and {}. Current value: {}'.format(Ny, y_min))
		raise ValueError('y_min must be between 0 and {}. Current value: {}'.format(Ny, y_min))

	if y_max is not None and (y_max < 0 or y_max > Ny):
		logging.error('y_max must be between 0 and {}. Current value: {}'.format(Ny, y_max))
		raise ValueError('y_max must be between 0 and {}. Current value: {}'.format(Ny, y_max))

	if z_min is not None and (z_min < 0 or z_min > Nz):
		logging.error('z_min must be between 0 and {}. Current value: {}'.format(Nz, z_min))
		raise ValueError('z_min must be between 0 and {}. Current value: {}'.format(Nz, z_min))

	if z_max is not None and (z_max < 0 or z_max > Nz):
		logging.error('z_max must be between 0 and {}. Current value: {}'.format(Nz, z_max))
		raise ValueError('z_max must be between 0 and {}. Current value: {}'.format(Nz, z_max))


def _check_args_values_ux(remove_uu, select_uu, NU, channel):
	for uu in remove_uu:
		if uu < 0 or uu >= NU:
			logging.error('remove_uu{} contains invalid data. {} is out of bound.'.format(channel, uu))
			raise ValueError('remove_uu{} contains invalid data. {} is out of bound.'.format(channel, uu))
		elif not isinstance(uu, int):
			logging.error('remove_uu{} contains invalid data. {} is not an int.'.format(channel, uu))
			raise TypeError('remove_uu{} contains invalid data. {} is not an int.'.format(channel, uu))

	if select_uu is not None and (select_uu < 0 or select_uu >= NU):
		logging.error('select_uu out of bound, should be between 0 and {}. It is {}.'.format(NU - 1, select_uu))
		raise ValueError('select_uu out of bound, should be between 0 and {}. It is {}.'.format(NU - 1, select_uu))


def _check_args_values_t(t_min, t_max, NP):
	if t_min is not None and (t_min < 0 or t_min > NP):
		logging.error('t_min must be between 0 and {}. Current value: {}'.format(NP, t_min))
		raise ValueError('t_min must be between 0 and {}. Current value: {}'.format(NP, t_min))

	if t_max is not None and (t_max < 0 or t_max > NP):
		logging.error('t_max must be between 0 and {}. Current value: {}'.format(NP, t_max))
		raise ValueError('t_max must be between 0 and {}. Current value: {}'.format(NP, t_max))

	if t_min is not None and t_max is not None and t_min >= t_max:
		logging.error('t_min must be smaller than t_max.')
		raise ValueError('t_min must be smaller than t_max.')


def parse_data(filename, t_baseline, correct_baseline=True, bm_smooth=250, correct_bm=True, x_min=None, x_max=None,
			   y_min=None, y_max=None, z_min=None, z_max=None, t_min=None, t_max=None,
			   select_channels=None, select_uu1=None, remove_uu1=(), select_uu2=None, remove_uu2=(),
			   axes_transpose='xyz', axes_flip='', max_points=500):
	"""Parses a raw TCT file produces by PSTCT and return the waveform, the beam monitor and metadata.
	The waveform and beam monitor data are returned as a numpy array, the metadata as a dictionary.
	The indices of the arrays are, in order, channel, U2, U1, z, y, x and time
	(time is not present in the beam monitor data).

	Before returning the arrays, it will correct the baseline of the waveforms, smooth the beam monitor data and use it
	to correct the waveforms, unless instructed not to do so.

	:param filename: file name of the raw data file to be parsed
	:param t_baseline: the portion of waveforms, in ns, that will be used to calculate the baseline.
	The signal signal should be starting at least t_baseline ns after the first point of the waveform.
	The metadata t0 will be set to -t_baseline.
	:param correct_baseline: apply the baseline correction to the data
	:param bm_smooth: samples used for the moving average to smooth the beam monitor data. 0 will deactivate the smoothing.
	:param correct_bm: apply the beam monitor correction to the data
	:param x_min:
	:param x_max: indices of x region of interest selection. Data will be returned for x index in the interval [x_min, x_max)
	:param y_min:
	:param y_max: same as for x
	:param z_min:
	:param z_max: same as for x
	:param t_min:
	:param t_max: indices of the waveform region of interest. Each waveform will be restricted to [t_min, t_max).
	The baseline will be calculated AFTER this selection.
	:param select_channels: index of the channels to be saved.
	:param select_uu1: index of the voltage point to be selected. This has priority on remove_uu1,
	meaning that if both are not None, the voltage point selected by select_uu1 will be returned,
	even if it has been removed
	:param remove_uu1: indices of the voltage points to be removed
	:param select_uu2: same as for U1
	:param remove_uu2: same as for U1
	:param axes_transpose: new order for the x, y and z axis.
	:param axes_flip: axes to be flipped. Can be any combination of x, y and z.
	:param max_points: points to be read in a block. Low values improve memory utilisation. [DEPRECATED]
	:return: a numpy array with the waveforms, a dictionary with the metadata and a numpy array with the beam monitor data.
	"""

	# Verification of the parameters passed to the function

	select_channels, remove_uu1, remove_uu2 = _check_args_initial(select_channels, remove_uu1, remove_uu2,
																  select_uu1, select_uu2, x_min, x_max, y_min, y_max,
						z_min, z_max, t_min, t_max, axes_transpose)

	with open(filename, 'rb') as f:
		version = _read_float_to_int(f)
		logging.debug('File version: {}'.format(version))

		if version not in _SUPPORTED_VERSIONS:
			logging.error('TCT file version {} not supported'.format(version))
			raise UnsupportedVersionError('TCT file version {} not supported'.format(version))

		date_tmp = []
		for i in range(6):
			date_tmp.append(_read_float_to_int(f))

		datetime = '{:04d}-{:02d}-{:02d}T{:02d}:{:02d}:{:02d}'.format(date_tmp[2], date_tmp[1], date_tmp[0],
																	  date_tmp[3], date_tmp[4], date_tmp[5])

		_ = _read_float_to_int(f)   # abstime, not used

		x0 = _read_float(f)
		dx = _read_float(f)
		Nx = _read_float_to_int(f)

		y0 = _read_float(f)
		dy = _read_float(f)
		Ny = _read_float_to_int(f)

		z0 = _read_float(f)
		dz = _read_float(f)
		Nz = _read_float_to_int(f)

		_check_args_values_xyz(x_min, x_max, y_min, y_max, z_min, z_max, Nx, Ny, Nz)

		channels = []

		for i in range(4):
			channels.append(_read_float_to_int(f))

		channels_selection = channels.copy()

		if select_channels is not None:
			s_channels = [0, 0, 0, 0]
			for i in select_channels:
				s_channels[i] = 1

			for i, sel in enumerate(s_channels):
				channels_selection[i] &= sel

		n_channels_initial = channels.count(1)
		n_channels_final = channels_selection.count(1)

		NU1 = _read_float_to_int(f)

		_check_args_values_ux(remove_uu1, select_uu1, NU1, 1)

		r_NU1 = len(remove_uu1)

		NU1 -= r_NU1

		U1 = []
		I1 = []

		for i in range(NU1 + r_NU1):
			_ = _read_float(f)

		if select_uu1 is not None:
			r_NU1 += NU1 - 1
			NU1 = 1

		NU2 = _read_float_to_int(f)

		_check_args_values_ux(remove_uu2, select_uu2, NU2, 2)

		r_NU2 = len(remove_uu2)

		NU2 -= r_NU2

		U2 = []
		I2 = []
		for i in range(NU2 + r_NU2):
			_ = _read_float(f)

		if select_uu2 is not None:
			r_NU2 += NU2 - 1
			NU2 = 1

		_ = _read_float(f)  # t0 (always 0?)
		t0 = t_baseline
		dt = _read_float(f)
		NP = _read_float_to_int(f)

		_check_args_values_t(t_min, t_max, NP)

		if t_min is None:
			t_min = 0

		if t_max is None:
			t_max = NP

		NP_new = t_max - t_min

		T = _read_float(f)
		source = _read_float_to_int(f)

		user = f.read(_read_int(f)).decode()
		sample = f.read(_read_int(f)).decode()
		comment = f.read(_read_int(f)).decode()

		logging.debug('user: {}'.format(user))
		logging.debug('sample: {}'.format(sample))
		logging.debug('comment: {}'.format(comment))

		logging.debug('x0: {}, dx: {}, Nx: {}'.format(x0, dx, Nx))
		logging.debug('y0: {}, dy: {}, Ny: {}'.format(y0, dy, Ny))
		logging.debug('z0: {}, dz: {}, Nz: {}'.format(z0, dz, Nz))

		logging.debug('NU1: {}'.format(NU1))
		logging.debug('NU2: {}'.format(NU2))

		logging.debug('t0: {}, dt: {}, NP {}'.format(t0, dt, NP))
		logging.debug('T: {}, source: {}'.format(T, source))

		waveforms = np.empty((n_channels_final, NU2, NU1, Nz, Ny, Nx, NP_new), dtype=np.float32)
		beam_monitor = np.empty((NU2, NU1, Nz, Ny, Nx), dtype=np.float32)

		n_removed_uu1 = 0
		n_removed_uu2 = 0

		if version == 51:
			header_size = 5
		elif version == 81:
			header_size = 8
		elif version == 82:
			header_size = 18
		else:
			logging.error('Unsupported TCT file version', version)
			raise UnsupportedVersionError('TCT file version {} not supported'.format(version))

		# The number of floats per each voltage
		n_elements = Nx * Ny * Nz * (header_size + NP * n_channels_initial)
		# The number of floats per each voltage, minus the header
		n_elements_clean = Nx * Ny * Nz * (NP_new * n_channels_initial)

		##############################
		# U2 loop
		for uu2 in range(NU2 + r_NU2):
			if select_uu2 is not None:
				if uu2 != select_uu2:
					# skip NU1 voltage blocks (+4 because U1, I1, U2, I2: *4 because float is 4 bytes)
					f.seek(NU1 * (n_elements + 4) * 4, 1)
					n_removed_uu2 += 1
					logging.info('skipping uu2: {}'.format(uu2))
					continue
			elif uu2 in remove_uu2:
				# skip NU1 voltage blocks (+4 because U1, I1, U2, I2: *4 because float is 4 bytes)
				f.seek(NU1 * (n_elements + 4) * 4, 1)
				n_removed_uu2 += 1
				logging.info('skipping uu2: {}'.format(uu2))
				continue

			# append U2 and I2 only the first time, so they will be NU2 items long
			append_uu2 = True

			##############################
			# U1 loop
			for uu1 in range(NU1 + r_NU1):
				logging.info('U1, U2: {} {}'.format(uu1, uu2))

				if select_uu1 is not None:
					if uu1 != select_uu1:
						f.seek((n_elements + 4) * 4, 1)  # skip this voltage block + U1, U2, I1, I2 (float is 4 bytes)
						n_removed_uu1 += 1
						logging.info('skipping uu1: {}'.format(uu1))
						continue
				elif uu1 in remove_uu1:
					f.seek((n_elements + 4)*4, 1)  # skip this voltage block + U1, U2, I1, I2 (float is 4 bytes)
					n_removed_uu1 += 1
					logging.info('skipping uu1: {}'.format(uu1))
					continue

				# U1 and I1 always appended, U2 and I2 only the first time
				U1.append(_read_float(f))
				if append_uu2:
					U2.append(_read_float(f))
				else:
					f.seek(4, 1)

				I1.append(_read_float(f))
				if append_uu2:
					I2.append(_read_float(f))
					append_uu2 = False
				else:
					f.seek(4, 1)

				n_max = max_points * (header_size + NP * n_channels_initial)  # how many waveforms read in each loop
				n_max_clean = max_points * (NP_new * n_channels_initial)  # same, without the header

				raw_data = np.empty(n_elements_clean, dtype='f4')
				bm = np.empty(Nx * Ny * Nz, dtype='f4')

				missing_floats = n_elements

				# This part allows to read and process (remove headers, correct baseline) only a certain
				# number of elements at a time.
				# This is needed because the part that deletes the headers makes copies of the arrays,
				# so it easily fills up the memory if it works on a whole file
				for blk in range(ceil(n_elements / n_max)):
					logging.debug('processing block {}/{}'.format(blk+1, n_elements//n_max+1))

					if missing_floats > n_max:
						raw_data_tmp = np.fromfile(f, dtype='f4', count=n_max, sep='')
						missing_floats -= n_max
					else:
						raw_data_tmp = np.fromfile(f, dtype='f4', count=missing_floats, sep='')

					bm_tmp = raw_data_tmp[3::(header_size + NP * n_channels_initial)].copy()

					# remove the header and select the desired time interval from raw data
					raw_data_tmp = raw_data_tmp.reshape((-1, header_size + NP * n_channels_initial))

					t_sel = []
					for i in range(n_channels_initial):
						t_sel.extend(np.r_[header_size+t_min+NP*i:header_size+t_max+NP*i])
					t_sel = np.array(t_sel)

					raw_data_tmp = raw_data_tmp[:, t_sel]

					if correct_baseline:
						raw_data_tmp = raw_data_tmp.reshape((-1, NP_new))
						last_bin = int(t_baseline / dt)
						if last_bin >= NP_new:
							raise ValueError('Baseline estimation failed. The selected time window is too small.')
						corrections = np.mean(raw_data_tmp[:, 0:last_bin], axis=1)
						raw_data_tmp -= corrections[:, None]
						raw_data_tmp = raw_data_tmp.reshape(-1)

					bm[blk*max_points:blk*max_points+bm_tmp.shape[0]] = bm_tmp

					raw_data[blk*n_max_clean:min((blk+1)*n_max_clean, n_elements_clean)-1] = \
						raw_data_tmp[:min(n_max_clean, n_elements_clean-blk*n_max_clean)-1]

				try:
					raw_data = raw_data.reshape((Nz, Ny, Nx, n_channels_initial, NP_new))
					bm = bm.reshape((Nz, Ny, Nx))
				except ValueError:
					logging.error('The number of waveforms in the input file does not match the metadata. Probably the '
								  'measurement is not complete.')
					raise EOFError('The number of waveforms in the input file does not match the metadata. Probably '
								   'the measurement is not complete.')

				if n_channels_final > 0:
					cnt_in = 0
					cnt_fin = 0
					for v_in, v_fin in zip(channels, channels_selection):
						if v_in:
							if v_fin:
								waveforms[cnt_fin, uu2 - n_removed_uu2, uu1 - n_removed_uu1] = raw_data[:, :, :, cnt_in]
								cnt_fin += 1
							cnt_in += 1

					beam_monitor[uu2 - n_removed_uu2, uu1 - n_removed_uu1] = bm

	# Smooth beam monitor
	beam_monitor = beam_monitor.ravel()
	_moving_average(beam_monitor, bm_smooth)

	# Apply beam monitor correction, if requested
	if 'Beam monitor ON' in comment and correct_bm:
		waveforms = waveforms.reshape((n_channels_final, -1, NP_new))
		waveforms /= beam_monitor[:, None]
		waveforms = waveforms.reshape((n_channels_final, NU2, NU1, Nz, Ny, Nx, NP_new))

	beam_monitor = beam_monitor.reshape((NU2, NU1, Nz, Ny, Nx))

	# Selects only the desired x, y, z interval and updates the metadata
	waveforms = waveforms[:, :, :, z_min:z_max, y_min:y_max, x_min:x_max, :]
	beam_monitor = beam_monitor[:, :, z_min:z_max, y_min:y_max, x_min:x_max]

	x0 += (x_min if x_min is not None else 0) * dx
	Nx = (x_max if x_max is not None else Nx) - (x_min if x_min is not None else 0)

	y0 += (y_min if y_min is not None else 0) * dy
	Ny = (y_max if y_max is not None else Ny) - (y_min if y_min is not None else 0)

	z0 += (z_min if z_min is not None else 0) * dz
	Nz = (z_max if z_max is not None else Nz) - (z_min if z_min is not None else 0)

	# Traspose and flip
	waveforms = np.transpose(waveforms, _TRANSPOSE_DICT_WFS[axes_transpose])
	beam_monitor = np.transpose(beam_monitor, _TRANSPOSE_DICT_BM[axes_transpose])

	if 'x' in axes_flip:
		waveforms = np.flip(waveforms, axis=5)
	if 'y' in axes_flip:
		waveforms = np.flip(waveforms, axis=4)
	if 'z' in axes_flip:
		waveforms = np.flip(waveforms, axis=3)

	# Create metadata and modify them according to the transposition

	metadata = {'user': user, 'datetime': datetime, 'x0': x0, 'dx': dx, 'Nx': Nx, 'y0': y0, 'dy': dy, 'Ny': Ny,
				'z0': z0, 'dz': dz, 'Nz': Nz, 'channels': channels_selection, 'NU1': NU1, 'U1': U1, 'I1': I1, 'NU2': NU2,
				'U2': U2, 'I2': I2, 't0': -t0, 'dt': dt, 'NP': NP_new, 'T': T, 'source': source,
				'comment': comment, 'sample': sample}

	if axes_transpose == 'xzy':
		metadata.update({'y0': z0, 'dy': dz, 'Ny': Nz, 'z0': y0, 'dz': dy, 'Nz': Ny})
	elif axes_transpose == 'yxz':
		metadata.update({'x0': y0, 'dx': dy, 'Nx': Ny, 'y0': x0, 'dy': dx, 'Ny': Nx})
	elif axes_transpose == 'yzx':
		metadata.update({'x0': y0, 'dx': dy, 'Nx': Ny, 'y0': z0, 'dy': dz, 'Ny': Nz, 'z0': x0, 'dz': dx, 'Nz': Nx})
	elif axes_transpose == 'zxy':
		metadata.update({'x0': z0, 'dx': dz, 'Nx': Nz, 'y0': x0, 'dy': dx, 'Ny': Nx, 'z0': y0, 'dz': dy, 'Nz': Ny})
	elif axes_transpose == 'zxy':
		metadata.update({'x0': z0, 'dx': dz, 'Nx': Nz, 'z0': x0, 'dz': dx, 'Nz': Nx})

	if n_channels_final > 0:
		return waveforms, metadata, beam_monitor
	else:
		logging.warning('Zero channels selected. Returning None, None, metadata')
		metadata.update({'channels': channels})
		return None, metadata, None
