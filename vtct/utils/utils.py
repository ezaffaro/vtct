from os import path, makedirs
import numpy as np
import logging

LOGGING_DICT = {
	'warning': logging.WARNING,
	'info': logging.INFO,
	'debug': logging.DEBUG
}


def create_path(p):
	if path.exists(p):
		logging.info('{} already exists.'.format(p))
	else:
		logging.info('{} does not exists. Creating it.'.format(p))
		makedirs(p)


# TODO make it work on any axis
def moving_average(array, samples):
	for i in range(array.shape[0]):
				array[i] = np.mean(array[max(0, i - samples):min(array.shape[0], i + samples)])
